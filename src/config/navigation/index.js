import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import {
  LoginScreen,
  DashboardScreen,
  OrganizationNameScreen,
  OrganizationPaymentScreen,
  ReviewScreen,
  OrganizationScreen,
  YardReview,
  YardScreen
} from "../../screens";
import { NotFound } from "../../components";

class BasicRouter extends React.Component {
  render() {
    return (
      <div>
        <Router>
          <Switch>
            <Route exact path="/" component={LoginScreen} />
            <Route path="/dashboard" component={DashboardScreen} />
            <Route
              path="/organization-name"
              component={OrganizationNameScreen}
            />
            <Route
              path="/organization-payment"
              component={OrganizationPaymentScreen}
            />
            <Route path="/organization-review" component={ReviewScreen} />
            <Route path="/yard-review" component={YardReview} />
            <Route path="/organization" component={OrganizationScreen} />
            <Route path="/yard" component={YardScreen} />
            <Route path="*" component={NotFound} />
          </Switch>
        </Router>
      </div>
    );
  }
}

export default BasicRouter;
