import React from "react";
// CSS
import "./login.css";
import {
  Image,
  Heading,
  Logo,
  FacebookButton,
  TwitterButton,
  Input,
  Button
} from "../../components";

// ICONS
import { FaUserCircle, FaKey } from "react-icons/fa";

class LoginScreen extends React.Component {
  constructor() {
    super();
    this.state = {
      userName: "",
      userPassword: ""
    };
  }

  //   Save Data Of From In State

  saveData = () => {
    let { userName, userPassword } = this.state;

    if (!userName) {
      alert("User Name is required");
    } else if (!userPassword) {
      alert("User Password is required");
    } else {
      console.log(userName, userPassword, "here is login data");
    }
  };

  render() {
    return (
      <div className="loginContainer">
        <div className="loginMainCard">
          <div className="loginFormCard">
            <Heading>SSG Fusion • Project Monarch</Heading>
            <Logo />
            <div>
              <Input
                type="text"
                placeholder="username"
                onChange={this.handleChange}
                id="username"
                icon={<FaUserCircle />}
              />
              <Input
                type="password"
                placeholder="password"
                onChange={this.handleChange}
                id="password"
                icon={<FaKey />}
              />
            </div>
            <div className="loginInfo">
              <p className="loginRegisterText">register online</p>
              <Button onClick={() => this.props.history.push("/dashboard")}>
                Login
              </Button>
            </div>
            <span className="loginOrText">Or</span>

            <div className="loginSocialContainer">
              <FacebookButton />
              <TwitterButton />
            </div>
          </div>
          <div>
            <Image path={require("../../assets/cow.png")} />
          </div>
        </div>
      </div>
    );
  }
}

export default LoginScreen;
