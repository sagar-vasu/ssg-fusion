import React from "react";
// CSS
import "./yard.css";
// Components
import {
  Header,
  Button,
  List,
  Chip,
  Card,
  Notification
} from "../../components";
// Icon
import { FaCheck } from "react-icons/fa";

class YardScreen extends React.Component {
  constructor() {
    super();
    this.state = {
      details: [
        { name: "Tangle Media Feeders" },
        { name: "Lethbridge, Alberta" },
        { name: "Canada" }
      ]
    };
  }
  render() {
    return (
      <div>
        <Header />
        <div className="yardBody">
          <div className="btnContainer">
            <Button id="yardBtn">Change owner</Button>
            <Button id="yardBtn">Reports</Button>
            <Button id="yardBtn">Manage</Button>
          </div>
          <List
            name="Home Quarter"
            info="Created on March 20, 2020 by Fergus Raphael"
          />
          <Button id="launchBtn"> LAUNCH</Button>
          <List name="Members" info="You can invite new members by email" />
          <div className="yardChips">
            <Card id="yardCard">
              <Chip backgroundColor="#4A90E2" name="SSG Fusion" badge="ADMIN" />
              <Chip backgroundColor="green" name="SSG Fusion" badge="ADMIN" />
              <Chip
                backgroundColor="red"
                name="Tangle Media Inc."
                badge="ADMIN"
              />
              <Button
                onClick={() => this.setState({ showYardModal: true })}
                id="yardMember"
              >
                Invite New Member
              </Button>
            </Card>
          </div>

          <div className="yardChips">
            <Card id="yardCard">
              <div className="organizationListInfo">
                <List
                  name="Billing details"
                  list={this.state.details}
                  listInfo={this.state.userListDetails}
                />
                <Notification
                  icon={<FaCheck className="notificationIcon" size={25} />}
                  title="44 days remaining in Free trial "
                />
              </div>
            </Card>
          </div>
        </div>
      </div>
    );
  }
}

export default YardScreen;
