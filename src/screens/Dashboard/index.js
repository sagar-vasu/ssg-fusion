import React from "react";
// CSS
import "./dashboard.css";
import { Header, List, Card, Chip, PlanCard } from "../../components";

class DashboardScreen extends React.Component {
  render() {
    return (
      <div>
        <Header />
        <div className="dashboardBody">
          <List
            name="Organizations"
            info="You are a member of the following billing entities"
          />
          <div className="dashboardChips">
            <Card>
              <Chip backgroundColor="#4A90E2" name="SSG Fusion" badge="ADMIN" />
              <Chip backgroundColor="green" name="SSG Fusion" badge="ADMIN" />
              <Chip
                backgroundColor="red"
                name="Tangle Media Inc."
                badge="ADMIN"
              />
            </Card>
          </div>

          <List
            name="Start your trial"
            info="This will create a new billable company in charge of administration for one or more yards"
          />
          <div
            className="dashboardChips"
            onClick={() => this.props.history.push("/organization-name")}
          >
            <Card>
              <p className="dashboardContent">
                This will create a new billable company in charge of
                administration for one or more yards
              </p>
              <PlanCard price="$199.99/mo" time="90 day trial" />
            </Card>
          </div>
        </div>
      </div>
    );
  }
}

export default DashboardScreen;
