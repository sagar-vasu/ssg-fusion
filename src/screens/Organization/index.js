import React from "react";
// CSS
import "./organization.css";
// Components
import {
  Header,
  List,
  Card,
  Chip,
  Button,
  Modal,
  Input,
  Notification
} from "../../components";

// Icons
import { FaUserAlt, FaRegEnvelope, FaLock, FaCheck } from "react-icons/fa";

class OrganizationScreen extends React.Component {
  constructor() {
    super();
    this.state = {
      showYardModal: false,
      showLinkModal: false,
      details: [
        { name: "Tangle Media Feeders" },
        { name: "Lethbridge, Alberta" },
        { name: "Canada" }
      ],
      userListDetails: [
        { name: "Fergus Raphael" },
        { name: "Visa" },
        { name: "4516  **** **** ****" },
        { name: "11/22" }
      ]
    };
  }

  // create yard func

  createYard = () => {
    this.setState({ showYardModal: false });
    this.props.history.push("yard-review");
  };

  render() {
    return (
      <div>
        <Header />
        <div className="organizationBody">
          <List
            name="Tangle Media Feeders"
            info="This will require a credit card billing agreement"
          />
          <div className="organizationChips">
            <Card id="organizationCard">
              <div className="organizationListInfo">
                <List
                  name="Billing details"
                  list={this.state.details}
                  listInfo={this.state.userListDetails}
                />
                <Notification
                  icon={<FaCheck className="notificationIcon" size={25} />}
                  title="44 days remaining in Free trial "
                />
              </div>
            </Card>
          </div>

          <List name="Yards" info="You have created 2 out of 5 yards" />
          <div className="organizationChips">
            <Card id="organizationCard">
              <Chip
                name="Home Quarter"
                onClick={() => this.props.history.push("/yard")}
              />
              <Chip
                onClick={() => this.props.history.push("/yard")}
                backgroundColor="#4A90E2"
                name="Riverbend Lot"
                badge="ADMIN"
              />
              <Modal show={this.state.showYardModal}>
                <List name="Create a yard" />
                <div className="organizationInputContainer">
                  <Input
                    type="text"
                    placeholder="yard name"
                    onChange={this.handleChange}
                    inputId="yardName"
                    icon={<FaUserAlt />}
                  />
                </div>
                <Button id="inviteMember" onClick={this.createYard}>
                  Next
                </Button>
              </Modal>

              <Button
                onClick={() => this.setState({ showYardModal: true })}
                id="inviteMember"
              >
                Add new yard
              </Button>
            </Card>
          </div>
          <List name="Members" info="You can invite new members by email" />
          <div className="organizationChips">
            <Card id="organizationCard">
              <Chip
                name="Fergus Raphael"
                badge="ADMIN"
                backgroundColor="#4A90E2"
                onClick={() => this.props.history.push("/yard")}
              />
              <Chip
                name="Matthew Wallocha"
                onClick={() => this.props.history.push("/yard")}
              />
              <Chip
                name="Max Koshney"
                onClick={() => this.props.history.push("/yard")}
              />
              <Chip
                name="Patrick Kelly"
                badge="GUEST"
                backgroundColor="green"
              />

              {/* invite new member */}

              <Modal show={this.state.showLinkModal}>
                <List name="Invite a new member to this yard" />
                <div className="organizationInputContainer">
                  <Input
                    type="text"
                    placeholder="member's name"
                    onChange={this.handleChange}
                    inputId="memberName"
                    icon={<FaUserAlt />}
                  />

                  <Input
                    type="text"
                    placeholder="email address"
                    onChange={this.handleChange}
                    inputId="email"
                    icon={<FaRegEnvelope />}
                  />

                  <Input
                    type="text"
                    placeholder="role"
                    onChange={this.handleChange}
                    inputId="role"
                    icon={<FaLock />}
                  />
                </div>
                <Button
                  id="inviteMember"
                  onClick={() => this.setState({ showLinkModal: false })}
                >
                  send invitation
                </Button>
              </Modal>

              <Button
                id="inviteMember"
                onClick={() => this.setState({ showLinkModal: true })}
              >
                Invite new member
              </Button>
            </Card>
          </div>
        </div>
      </div>
    );
  }
}

export default OrganizationScreen;
