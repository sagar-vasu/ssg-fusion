import React from "react";
// CSS
import "./yardReview.css";
// Components
import { Header, List, Card, Button } from "../../components";

class YardReview extends React.Component {
  render() {
    return (
      <div>
        <Header />
        <div className="yardReviewBody">
          <List
            name="Review your yard"
            info="This will require a credit card billing agreement"
          />
          <div className="yardReviewChips">
            <Card id="yardReviewCard">
              <div className="yardReviewInsideList">
                <List name="Yard name" id="yardReviewCard" />
                <p>Home let</p>
              </div>
            </Card>
          </div>
          <Button
            id="reviewYardbtn"
            onClick={() => this.props.history.push("/organization")}
          >
            Create Yard
          </Button>
        </div>
      </div>
    );
  }
}

export default YardReview;
