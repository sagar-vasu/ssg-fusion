import React from "react";
// CSS
import "./organizationPayment.css";
// Components
import { Header, List, Card, Input, PlanCard, Button } from "../../components";
// ICONS
import { FaUserAlt, FaRegCreditCard, FaCalendar, FaLock } from "react-icons/fa";

class OrganizationPaymentScreen extends React.Component {
  constructor() {
    super();
    this.state = {
      details: [
        { name: "Tangle Media Feeders" },
        { name: "Lethbridge, Alberta" },
        { name: "Canada" }
      ]
    };
  }
  render() {
    return (
      <div>
        <Header />
        <div className="organizationPaymentBody">
          <List
            name="Start your free trial!"
            info="This will require a credit card billing agreement"
          />
          <div className="organizationPaymentChips">
            <Card id="organizationPaymentCard">
              <PlanCard
                id="organizationPaymentPlan"
                price="$199.99/mo"
                time="90 day trial"
              />
              <div className="organizationPaymentInsideList">
                <List
                  name="Organization details"
                  id="listHeading"
                  list={this.state.details}
                />
              </div>
            </Card>
          </div>
          <List
            name="Just a few questions to get started."
            info="Your card will not be charged until the end of your free trial."
          />
          <div className="organizationPaymentChips">
            <Card id="organizationPaymentCard">
              <Input
                id="organizationPaymentinput"
                type="text"
                placeholder="Name on Card"
                onChange={this.handleChange}
                inputId="nameOnCard"
                icon={<FaUserAlt />}
              />

              <Input
                id="organizationPaymentinput"
                type="text"
                placeholder="Card type"
                onChange={this.handleChange}
                inputId="cardType"
                icon={<FaRegCreditCard />}
              />
              <Input
                id="organizationPaymentinput"
                type="number"
                placeholder="____ - ____ - ____ - ____"
                onChange={this.handleChange}
                inputId="cardNo"
                icon={<FaCalendar />}
              />
              <Input
                id="organizationPaymentinput"
                type="text"
                placeholder="MM / YY"
                onChange={this.handleChange}
                inputId="date"
                icon={<FaRegCreditCard />}
              />
              <Input
                id="organizationPaymentinput"
                type="text"
                placeholder="CVV"
                onChange={this.handleChange}
                inputId="cvc"
                icon={<FaLock />}
              />
              <Button
                id="reviewButton"
                onClick={() => this.props.history.push("/organization-review")}
              >
                Review
              </Button>
            </Card>
          </div>
        </div>
      </div>
    );
  }
}

export default OrganizationPaymentScreen;
