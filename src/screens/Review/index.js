import React from "react";
// CSS
import "./review.css";
// Components
import { Header, List, Card, PlanCard, Button } from "../../components";

class ReviewScreen extends React.Component {
  constructor() {
    super();
    this.state = {
      details: [
        { name: "Tangle Media Feeders" },
        { name: "Lethbridge, Alberta" },
        { name: "Canada" }
      ],
      userListDetails: [
        { name: "Fergus Raphael" },
        { name: "Visa" },
        { name: "4516  **** **** ****" },
        { name: "11/22" }
      ]
    };
  }
  render() {
    return (
      <div>
        <Header />
        <div className="reviewBody">
          <List
            name="Start your free trial!"
            info="This will require a credit card billing agreement"
          />
          <div className="reviewChips">
            <Card id="reviewCard">
              <PlanCard
                id="reviewPlan"
                price="$199.99/mo"
                time="90 day trial"
              />
              <div className="reviewInsideList">
                <List
                  name="Billing details"
                  list={this.state.details}
                  listInfo={this.state.userListDetails}
                />
              </div>
              <Button
                onClick={() => this.props.history.push("/organization")}
                id="startTrailButton"
              >
                Start my trial
              </Button>
            </Card>
          </div>
        </div>
      </div>
    );
  }
}

export default ReviewScreen;
