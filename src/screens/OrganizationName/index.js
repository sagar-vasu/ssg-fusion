import React from "react";
// CSS
import "./organizationName.css";
// Components
import { Header, List, Card, Input, PlanCard, Button } from "../../components";
// ICONS
import { FaStore, FaMapMarkerAlt, FaMap, FaGlobe } from "react-icons/fa";

class OrganizationNameScreen extends React.Component {
  constructor() {
    super();
    this.state = {
      name: ""
    };
  }
  render() {
    return (
      <div>
        <Header />
        <div className="organizationNameBody">
          <List
            name="Start your free trial!"
            info="This will require a credit card billing agreement"
          />
          <div className="organizationNameChips">
            <Card id="organizationNameCard">
              <PlanCard
                id="organizationNamePlan"
                price="$199.99/mo"
                time="90 day trial"
              />
            </Card>
          </div>
          <div className="organizationNameChips">
            <List name="Just a few questions to get started." />
            <Card id="organizationNameCard">
              <Input
                id="organizationNameinput"
                type="text"
                placeholder="Business Name"
                onChange={this.handleChange}
                inputId="businessName"
                icon={<FaStore />}
              />

              <Input
                id="organizationNameinput"
                type="text"
                placeholder="City"
                onChange={this.handleChange}
                inputId="city"
                icon={<FaMapMarkerAlt />}
              />
              <Input
                id="organizationNameinput"
                type="text"
                placeholder="State/Province"
                onChange={this.handleChange}
                inputId="state"
                icon={<FaMap />}
              />
              <Input
                id="organizationNameinput"
                type="text"
                placeholder="Country"
                onChange={this.handleChange}
                inputId="country"
                icon={<FaGlobe />}
              />
              <Button
                id="nextButton"
                onClick={() => this.props.history.push("/organization-payment")}
              >
                Next
              </Button>
            </Card>
          </div>
        </div>
      </div>
    );
  }
}

export default OrganizationNameScreen;
