import React from "react";
// CSS
import "./chip.css";

class Chip extends React.Component {
  render() {
    return (
      <div style={{ display: "flex" }} onClick={this.props.onClick}>
        <div className="chipContainer">
          <span className="chipCircle"></span>
          <span className="chipTitle">{this.props.name}</span>
          <span
            className="chipBadge"
            style={{ backgroundColor: this.props.backgroundColor }}
          >
            {this.props.badge}
          </span>
        </div>
      </div>
    );
  }
}

export default Chip;
