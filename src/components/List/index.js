import React from "react";
// CSS
import "./list.css";

class ListInfo extends React.Component {
  render() {
    return (
      <div className="list">
        <h3 id={this.props.id}>{this.props.name}</h3>
        <span>{this.props.info}</span>
        <div className="ulList">
          {this.props.list ? (
            <ul>
              {this.props.list &&
                this.props.list.map((val, i) => {
                  return <li key={i}> {val.name} </li>;
                })}
            </ul>
          ) : null}
          {this.props.listInfo ? (
            <ul>
              {this.props.listInfo &&
                this.props.listInfo.map((val, i) => {
                  return <li key={i}> {val.name} </li>;
                })}
            </ul>
          ) : null}
        </div>
      </div>
    );
  }
}

export default ListInfo;
