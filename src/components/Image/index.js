import React from "react";
// CSS
import "./image.css";

class Image extends React.Component {
  render() {
    return <img src={this.props.path} className="img" alt="logo" />;
  }
}

export default Image;
