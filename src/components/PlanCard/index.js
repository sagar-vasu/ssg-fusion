import React from "react";
// CSS
import "./planCard.css";
// ICONS
import { FaRegGem } from "react-icons/fa";

class PlanCard extends React.Component {
  render() {
    return (
      <div className="planCard" id={this.props.id}>
        <div className="planHeader">
          <FaRegGem color="white" size={25} />
          <h6>Premiere Plan</h6>
        </div>
        <div className="planFooter">
          <p className="planFooterText">{this.props.price}</p>
          <p className="planFooterTime">{this.props.time}</p>
        </div>
      </div>
    );
  }
}

export default PlanCard;
