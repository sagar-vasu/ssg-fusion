import React from "react";
// CSS
import "./header.css";
// Component
import { Logo, Heading } from "../";
// ICONS
import { MdClose } from "react-icons/md";

class Header extends React.Component {
  render() {
    return (
      <div className="mainHeader">
        <div className="headerLogo">
          <Logo />
          <Heading id="headerTitle">SSG Fusion • Project Monarch</Heading>
        </div>
        <div className="headerIcon">
          <MdClose color="#783011" size={32} />
        </div>
      </div>
    );
  }
}

export default Header;
