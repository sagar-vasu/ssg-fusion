import React from "react";
import "./heading.css";

class Heading extends React.Component {
  render() {
    return (
      <h2 id={this.props.id} className="heading">
        {this.props.children}
      </h2>
    );
  }
}

export default Heading;
