import React from "react";
// CSS
import "./facebook.css";
// ICONS
import { FaFacebookSquare } from "react-icons/fa";

class FacebookButton extends React.Component {
  render() {
    return (
      <div className="fbBtnContainer">
        <button className="fbButton">
          <FaFacebookSquare className="fbIcon" color="#FFFFFF" size={30} />
          <span className="btnText">FaceBook</span>
        </button>
      </div>
    );
  }
}

export default FacebookButton;
