import React from "react";
// CSS
import "./notification.css";

class NotificationScreen extends React.Component {
  render() {
    return (
      <div className="notification">
        {this.props.icon}
        <p className="notificationInfo">{this.props.title}</p>
      </div>
    );
  }
}

export default NotificationScreen;
