import React from "react";
import { Modal } from "react-bootstrap";

class MyVerticallyCenteredModal extends React.Component {
  constructor() {
    super();
    this.state = {
      hide: true
    };
  }
  render() {
    return (
      <Modal
        size="lg"
        show={this.props.show}
        centered
      >
        <Modal.Body>{this.props.children}</Modal.Body>
      </Modal>
    );
  }
}

export default MyVerticallyCenteredModal;
