import React from "react";
// CSS
import "./input.css";

import { Form, Col, InputGroup } from "react-bootstrap";

class Input extends React.Component {
  render() {
    return (
      <div id={this.props.id} className="input">
        <Form.Group as={Col} md="10" xs={10}>
          <InputGroup>
            <InputGroup.Prepend>
              <InputGroup.Text className="iconInput">
                {this.props.icon}
              </InputGroup.Text>
            </InputGroup.Prepend>
            <Form.Control
              className="customInputCss"
              id={this.props.inputId}
              type={this.props.type}
              placeholder={this.props.placeholder}
              aria-describedby="inputGroupPrepend"
              value={this.props.value}
              onChange={this.props.onChange}
              style={{
                width: this.props.width
              }}
            />
          </InputGroup>
        </Form.Group>
      </div>
    );
  }
}

export default Input;
