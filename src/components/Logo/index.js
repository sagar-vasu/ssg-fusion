import React from "react";
// CSS
import "./logo.css";
// ICON
import { GiButterfly } from "react-icons/gi";

class Logo extends React.Component {
  render() {
    return (
      <div className="logoContainer">
        <GiButterfly size={40} color="#F5A623" />
      </div>
    );
  }
}

export default Logo;
