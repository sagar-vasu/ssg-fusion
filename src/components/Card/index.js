import React from "react";
// CSS
import "./card.css";

class Card extends React.Component {
  render() {
    return (
      <div id={this.props.id} className="cardContainer">
        {this.props.children}
      </div>
    );
  }
}

export default Card;
