import React from "react";
import "./404.css";

class Notfound extends React.Component {
  render() {
    return (
      <div>
        <div class="notfound">Sorry! Page not found</div>
      </div>
    );
  }
}

export default Notfound;
