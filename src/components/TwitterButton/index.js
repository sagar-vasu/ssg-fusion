import React from "react";
// CSS
import "./twitter.css";
// ICONS
import { FaTwitterSquare } from "react-icons/fa";

class TwitterButton extends React.Component {
  render() {
    return (
      <div className="twiterBtnContainer">
        <button className="twiterButton">
          <FaTwitterSquare className="twiterIcon" color="#FFFFFF" size={30} />
          <span className="twitterBtnText">Twitter</span>
        </button>
      </div>
    );
  }
}

export default TwitterButton;
