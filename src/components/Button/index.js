import React from "react";
// CSS
import "./button.css";

class Button extends React.Component {
  render() {
    return (
      <button
        onClick={this.props.onClick}
        id={this.props.id}
        className="customButton"
      >
        {this.props.children}
      </button>
    );
  }
}

export default Button;
